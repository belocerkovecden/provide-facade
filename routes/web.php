<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Тест провайдера
Route::get('/calc', [IndexController::class, 'calculate'])->name('calc');
Route::get('/calcFacade', [IndexController::class, 'calcFacade'])->name('calcFacade');
