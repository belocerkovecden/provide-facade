<?php

if (!function_exists("helloWorld")) {
    /**
     * @return string
     */
    function helloWorld(): string
    {
        return "Hello, World!";
    }
}
