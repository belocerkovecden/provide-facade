<?php

namespace App\Http\Controllers;

use App\Facade\Price;
use App\Services\PriceCalculateService;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function calculate(Request $request, PriceCalculateService $service)
    {
        //dd($request->all());
        dd($service->start($request->all(), 'plus'));
        //return response()->view('index');
    }

    public function calcFacade(Request $request)
    {
        //dd(app('price')->start($request->all())); // price имя фасада
        //dd(Price::start($request->all())); // price имя фасада
        //dd(Price::start($request->all(), 'minus')); // price имя фасада

        return response()->view('index');
    }
}
