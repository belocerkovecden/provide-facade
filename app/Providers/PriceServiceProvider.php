<?php

namespace App\Providers;

use App\Services\PriceCalculateService;
use Illuminate\Support\ServiceProvider;

class PriceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        // Регистрация для провайдеров
        /*$this->app->bind(PriceCalculateService::class, function ($app) {
            return new PriceCalculateService();
        });*/
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        // Регистрация для фасада
        $this->app->bind('price', PriceCalculateService::class);
    }
}
