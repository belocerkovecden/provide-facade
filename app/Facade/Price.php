<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;

class Price extends Facade
{

    /**
     * @see \App\Services\PriceCalculateService
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'price';
    }
}
